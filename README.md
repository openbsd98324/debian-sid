# debian-sid

debian sid rootfs


https://cdimage.debian.org/cdimage/weekly-builds/

Software:
The latest Debian ARM64 net install ISO.

You can download it from https://cdimage.debian.org/debian-cd/current/arm64/iso-cd/
(debian-##.#.#-arm64-netinst.iso, 250 MB).
The latest Raspberry Pi 3 UEFI firmware binary, along with the relevant Broadcom bootloader support files (i.e. bootcode.bin, config.txt, fixup.dat, start.elf).

You can find a ready-to-use archive with all of the above at https://github.com/pftf/RPi3/releases
(RPi3_UEFI_Firmware_v#.##.zip, 3 MB).

Note that this firmware archive works for both the Raspberry Pi 3 Model B and the Raspberry Pi 3 Model B+ (as the relevant Device Tree is automatically selected during boot).
(Optional) The non-free WLAN firmware binaries that are needed if you want to use Wifi for the installation.
Note that, if you picked up the archive above then you don't need to do anything as the WLAN firmware binaries are included in it too.



Partition your SD media as MBR and create a single partition of 300 MB of type 0x0e (FAT16 with LBA).
Do not be tempted to use GPT as the partition scheme or 0xef (ESP) for the partition type, as the ondie Broadcom bootloader does not support any of those. It must be MBR and type 0x0e. You can use the command line utilities fdisk on Linux or DISKPART on Windows to do that.
Set the partition as active/bootable. This is very important as, otherwise, the Debian partition manager will not automatically detect it as ESP (EFI System Partition) which will create problems that you have to manually resolve (See Appendix C).
If using fdisk on Linux, you can use a to set the partition as active.
If using Windows, you can use DISKPART and then type the command active after selecting the relevant disk and partition.
Format the partition as FAT16. It MUST be FAT16 and not FAT32, as the Debian partition manager will not detect it as ESP otherwise and, again, you will have to perform extra steps to salvage your system before reboot (Appendix C).
The Linux and Windows base utilities should be smart enough to use FAT16 and not FAT32 for a 300 MB partition, so you should simply be able to use mkfs.vfat /dev/<yourdevice> (Linux) or format fs=fat quick in Windows' DISKPART. The Windows Disk Manager should also be smart enough to use FAT16 instead of FAT32 if you decide to use it to format the partition.
Extract the UEFI bootloader support files mentioned above to the newly formatted FAT partition. If you downloaded the Raspberry Pi 3 UEFI firmware binary from the link above, you just have to uncompress the zip file onto the root of your media, and everything will be set as it should be.
Extract the content of the Debian ISO you downloaded to the root of the FAT partition. On Windows you can use a utility such as 7-zip to do just that (or you can mount the ISO in File Explorer then copy the files).


https://raspi.debian.net/daily-images/


